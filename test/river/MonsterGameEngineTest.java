package river;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

public class MonsterGameEngineTest {
    private GameEngine engine;

    @Before
    public void setUp() {
        engine = new MonsterGameEngine();
    }

    @Test
    public void testObjectCallThroughs() {
        Assert.assertEquals("K3", engine.getItemLabel(Item.ITEM_5));
        Assert.assertEquals(Location.START, engine.getItemLocation(Item.ITEM_5));
        Assert.assertEquals(Color.GREEN, engine.getItemColor(Item.ITEM_5));

        Assert.assertEquals("M3", engine.getItemLabel(Item.ITEM_4));
        Assert.assertEquals(Location.START, engine.getItemLocation(Item.ITEM_4));
        Assert.assertEquals(Color.CYAN, engine.getItemColor(Item.ITEM_4));

        Assert.assertEquals("K2", engine.getItemLabel(Item.ITEM_3));
        Assert.assertEquals(Location.START, engine.getItemLocation(Item.ITEM_3));
        Assert.assertEquals(Color.GREEN, engine.getItemColor(Item.ITEM_3));

        Assert.assertEquals("M2", engine.getItemLabel(Item.ITEM_2));
        Assert.assertEquals(Location.START, engine.getItemLocation(Item.ITEM_2));
        Assert.assertEquals(Color.CYAN, engine.getItemColor(Item.ITEM_2));

        Assert.assertEquals("K1", engine.getItemLabel(Item.ITEM_1));
        Assert.assertEquals(Location.START, engine.getItemLocation(Item.ITEM_1));
        Assert.assertEquals(Color.GREEN, engine.getItemColor(Item.ITEM_1));

        Assert.assertEquals("M1", engine.getItemLabel(Item.ITEM_0));
        Assert.assertEquals(Location.START, engine.getItemLocation(Item.ITEM_0));
        Assert.assertEquals(Color.CYAN, engine.getItemColor(Item.ITEM_0));
    }

    @Test
    public void testWinningGame() {
        engine.loadBoat(Item.ITEM_0);
        engine.loadBoat(Item.ITEM_2);
        engine.rowBoat();   // 1 crossing

        engine.unloadBoat(Item.ITEM_0);
        engine.rowBoat();   // 2 crossing

        engine.loadBoat(Item.ITEM_4);
        engine.rowBoat();   // 3 crossing

        engine.unloadBoat(Item.ITEM_2);
        engine.rowBoat();   // 4 crossing

        engine.unloadBoat(Item.ITEM_4);
        engine.loadBoat(Item.ITEM_1);
        engine.loadBoat(Item.ITEM_3);
        engine.rowBoat();   // 5 crossing

        engine.unloadBoat(Item.ITEM_1);
        engine.loadBoat(Item.ITEM_2);
        engine.rowBoat();   // 6 crossing

        engine.unloadBoat(Item.ITEM_2);
        engine.loadBoat(Item.ITEM_5);
        engine.rowBoat();   // 7 crossing

        engine.unloadBoat(Item.ITEM_3);
        engine.unloadBoat(Item.ITEM_5);
        engine.loadBoat(Item.ITEM_0);
        engine.rowBoat();   // 8 crossing

        engine.loadBoat(Item.ITEM_2);
        engine.rowBoat();   // 9 crossing

        engine.unloadBoat(Item.ITEM_0);
        engine.rowBoat();   // 10 crossing

        engine.loadBoat(Item.ITEM_4);
        engine.rowBoat();   // 11 crossing

        engine.unloadBoat(Item.ITEM_2);
        engine.unloadBoat(Item.ITEM_4);

        Assert.assertTrue(engine.gameIsWon());
        Assert.assertFalse(engine.gameIsLost());
    }

    @Test
    public void testLosingGameTransportOneMunchkin() {
        engine.loadBoat(Item.ITEM_5);
        Assert.assertFalse(engine.gameIsLost());
        Assert.assertFalse(engine.gameIsWon());

        engine.rowBoat();
        Assert.assertTrue(engine.gameIsLost());
        Assert.assertFalse(engine.gameIsWon());
    }

    @Test
    public void testLosingGameMunchkinOnBoat() {
        engine.loadBoat(Item.ITEM_0);
        engine.loadBoat(Item.ITEM_2);
        engine.rowBoat();   // 1 crossing

        Assert.assertFalse(engine.gameIsLost());
        Assert.assertFalse(engine.gameIsWon());

        engine.unloadBoat(Item.ITEM_0);
        engine.rowBoat();   // 2 crossing

        Assert.assertFalse(engine.gameIsLost());
        Assert.assertFalse(engine.gameIsWon());

        engine.loadBoat(Item.ITEM_4);
        engine.rowBoat();   // 3 crossing

        Assert.assertFalse(engine.gameIsLost());
        Assert.assertFalse(engine.gameIsWon());

        engine.unloadBoat(Item.ITEM_2);
        engine.rowBoat();   // 4 crossing

        Assert.assertFalse(engine.gameIsLost());
        Assert.assertFalse(engine.gameIsWon());

        engine.unloadBoat(Item.ITEM_4);
        engine.loadBoat(Item.ITEM_1);
        engine.rowBoat(); // 5 crossing

        Assert.assertTrue(engine.gameIsLost());
        Assert.assertFalse(engine.gameIsWon());
    }
}
