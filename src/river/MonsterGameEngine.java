package river;

import java.awt.*;

public class MonsterGameEngine extends AbstractGameEngine {

    public static final Item MONSTER_1 = Item.ITEM_0;
    public static final Item MUNCHKIN_1 = Item.ITEM_1;
    public static final Item MONSTER_2 = Item.ITEM_2;
    public static final Item MUNCHKIN_2 = Item.ITEM_3;
    public static final Item MONSTER_3 = Item.ITEM_4;
    public static final Item MUNCHKIN_3 = Item.ITEM_5;

    public MonsterGameEngine() {
        super();
        gameObjects.put(MONSTER_1, new GameObject("M1", Location.START, Color.CYAN, true));
        gameObjects.put(MUNCHKIN_1, new GameObject("K1", Location.START, Color.GREEN, true));
        gameObjects.put(MONSTER_2, new GameObject("M2", Location.START, Color.CYAN, true));
        gameObjects.put(MUNCHKIN_2, new GameObject("K2", Location.START, Color.GREEN, true));
        gameObjects.put(MONSTER_3, new GameObject("M3", Location.START, Color.CYAN, true));
        gameObjects.put(MUNCHKIN_3, new GameObject("K3", Location.START, Color.GREEN, true));
    }

    @Override
    public boolean gameIsLost() {
        int munchkinsOnStart = 0;
        int monstersOnStart = 0;
        int munchkinsAtFinish = 0;
        int monstersAtFinish = 0;
        for (Item item : getItems()) {
            if (getItemLocation(item) == Location.START) {
                if (getItemLabel(item).startsWith("M")) {
                    monstersOnStart++;
                } else {
                    munchkinsOnStart++;
                }
            } else if (getItemLocation(item) == Location.FINISH) {
                if (getItemLabel(item).startsWith("M")) {
                    monstersAtFinish++;
                } else {
                    munchkinsAtFinish++;
                }
            } else {
                if (getBoatLocation().isAtStart()) {
                    if (getItemLabel(item).startsWith("M")) {
                        monstersOnStart++;
                    } else {
                        munchkinsOnStart++;
                    }
                } else {
                    if (getItemLabel(item).startsWith("M")) {
                        monstersAtFinish++;
                    } else {
                        munchkinsAtFinish++;
                    }
                }
            }
        }
        return (munchkinsOnStart > 0 && monstersOnStart > munchkinsOnStart) ||
                (munchkinsAtFinish > 0 && monstersAtFinish > munchkinsAtFinish);
    }
}
