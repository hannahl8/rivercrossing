package river;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class RiverGUI extends JPanel implements MouseListener {

    // ==========================================================
    // Fields (hotspots)
    // ==========================================================

    private Rectangle item0Rectangle = new Rectangle();
    private Rectangle item1Rectangle = new Rectangle();
    private Rectangle item2Rectangle = new Rectangle();
    private Rectangle item3Rectangle = new Rectangle();
    private Rectangle item4Rectangle = new Rectangle();
    private Rectangle item5Rectangle = new Rectangle();
    private Rectangle boatRectangle = new Rectangle();
    private final Rectangle farmerGameButtonRect = new Rectangle(300, 120, 100, 30);
    private final Rectangle monsterGameButtonRect = new Rectangle(410, 120, 100, 30);

    // ==========================================================
    // Private Fields
    // ==========================================================

    private GameEngine engine; // Declare
    private Graphics g;

    // ==========================================================
    // Constructor
    // ==========================================================

    public RiverGUI() {
        engine = new FarmerGameEngine();    // Initialize
        addMouseListener(this);
    }

    // ==========================================================
    // Paint Methods (View)
    // ==========================================================


    @Override
    public void paintComponent(Graphics g) {
        this.g = g;

        int count = 0;
        for (Item item : engine.getItems()) {
            if (engine.getItemLocation(item).isOnBoat()) {
                count++;
            }
            refreshItemRectangle(item, count);
        }
        refreshBoatRectangle(); // based on model

        g.setColor(Color.GRAY);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        for (Item item : engine.getItems()) {
            paintItem(item);
        }
        paintBoat();
        if (gameOver()) {
            paintRestartButtons();
        }
    }

    private void paintRestartButtons() {
        g.setColor(Color.BLACK);
        g.fillRect(farmerGameButtonRect.x - 3, farmerGameButtonRect.y - 3, farmerGameButtonRect.width + (2 * 3), farmerGameButtonRect.height + (2 * 3));
        g.setColor(Color.PINK);
        paintRectangle(Color.PINK, "Farmer", farmerGameButtonRect);

        g.setColor(Color.BLACK);
        g.fillRect(monsterGameButtonRect.x - 3, monsterGameButtonRect.y - 3, monsterGameButtonRect.width + (2 * 3), monsterGameButtonRect.height + (2 * 3));
        g.setColor(Color.PINK);
        paintRectangle(Color.PINK, "Monster", monsterGameButtonRect);
    }

    private boolean gameOver() {
        String message = "";
        if (engine.gameIsLost()) {
            message = "You Lost!";
            paintMessage(message);
            return true;
        } else if (engine.gameIsWon()) {
            message = "You Won!";
            paintMessage(message);
            return true;
        }
        return false;
    }

    private void paintMessage(String message) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Verdana", Font.BOLD, 36));
        FontMetrics fm = g.getFontMetrics();
        int strXCoord = 400 - fm.stringWidth(message) / 2;
        int strYCoord = 100;
        g.drawString(message, strXCoord, strYCoord);
    }

    /**
     * Refreshes the item rectangles based on the model.
     */
    private void refreshItemRectangle(Item item, int count) {
        int leftBaseX = 20;
        int leftBaseY = 275;
        int leftBoatX = 140;
        int leftBoatY = 275;
        int rightBaseX = 670;
        int rightBaseY = 275;
        int rightBoatX = 550;
        int rightBoatY = 275;
        Rectangle updatedRectangle = new Rectangle();
        int[] dx = {0, 60, 0, 60, 0, 60};
        int[] dy = {0, 0, -60, -60, -120, -120};
        int index = item.ordinal();

        if (engine.getItemLocation(item).isAtStart()) {
            // item on left shore
            updatedRectangle = new Rectangle(
                    leftBaseX + dx[index],
                    leftBaseY + dy[index],
                    50, 50
            );
        } else if (engine.getItemLocation(item).isAtFinish()) {
            // item on right shore
            updatedRectangle = new Rectangle(
                    rightBaseX + dx[index],
                    rightBaseY + dy[index],
                    50, 50
            );
        } else {
            if (engine.getBoatLocation().isAtStart()) {
                // item on left boat
                if (count == 1)
                    updatedRectangle = new Rectangle(
                            leftBoatX,
                            leftBoatY - 60,
                            50, 50
                    );
                else if (count == 2)
                    updatedRectangle = new Rectangle(
                            leftBoatX + 60,
                            leftBoatY - 60,
                            50, 50
                    );
            } else {
                // item on right boat
                if (count == 1)
                    updatedRectangle = new Rectangle(
                            rightBoatX,
                            rightBoatY - 60,
                            50, 50
                    );
                else if (count == 2)
                    updatedRectangle = new Rectangle(
                            rightBoatX + 60,
                            rightBoatY - 60,
                            50, 50
                    );
            }
        }
        switch (item) {
            case ITEM_0:
                item0Rectangle = updatedRectangle;
                break;
            case ITEM_1:
                item1Rectangle = updatedRectangle;
                break;
            case ITEM_2:
                item2Rectangle = updatedRectangle;
                break;
            case ITEM_3:
                item3Rectangle = updatedRectangle;
                break;
            case ITEM_4:
                item4Rectangle = updatedRectangle;
                break;
            case ITEM_5:
                item5Rectangle = updatedRectangle;
                break;
            default:
        }
    }

    private void refreshBoatRectangle() {
        Rectangle updatedBoat;
        int x;
        int y = 275;
        int width = 110;
        int height = 50;
        if (engine.getBoatLocation().isAtFinish()) {
            x = 550;
        } else {
            x = 140;
        }
        updatedBoat = new Rectangle(x, y, width, height);
        boatRectangle = updatedBoat;
    }

    private void paintItem(Item item) {
        switch (item) {
            case ITEM_0:
                paintRectangle(engine.getItemColor(item), engine.getItemLabel(item), item0Rectangle);
                break;
            case ITEM_1:
                paintRectangle(engine.getItemColor(item), engine.getItemLabel(item), item1Rectangle);
                break;
            case ITEM_2:
                paintRectangle(engine.getItemColor(item), engine.getItemLabel(item), item2Rectangle);
                break;
            case ITEM_3:
                paintRectangle(engine.getItemColor(item), engine.getItemLabel(item), item3Rectangle);
                break;
            case ITEM_4:
                paintRectangle(engine.getItemColor(item), engine.getItemLabel(item), item4Rectangle);
                break;
            case ITEM_5:
                paintRectangle(engine.getItemColor(item), engine.getItemLabel(item), item5Rectangle);
                break;
            default:
        }
    }

    private void paintBoat() {
        paintRectangle(Color.ORANGE, "", boatRectangle);
    }

    private void paintRectangle(Color color, String label, Rectangle rect) {
        // set color for fill
        g.setColor(color);
        g.fillRect(rect.x, rect.y, rect.width, rect.height);
        // set color for string
        g.setColor(Color.BLACK);
        int fontSize = (rect.height >= 40) ? 28 : 18;
        g.setFont(new Font("Verdana", Font.BOLD, fontSize));
        FontMetrics fm = g.getFontMetrics();
        int strXCoord = rect.x + rect.width / 2 - fm.stringWidth(label) / 2;
        int strYCoord = rect.y + rect.height / 2 + fontSize / 2 - 4;
        g.drawString(label, strXCoord, strYCoord);
    }

    // ==========================================================
    // Startup Methods
    // ==========================================================

    /**
     * Create the GUI and show it. For thread safety, this method should be invoked
     * from the event-dispatching thread.
     */
    private static void createAndShowGUI() {

        // Create and set up the window
        JFrame frame = new JFrame("RiverCrossing");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane
        RiverGUI newContentPane = new RiverGUI();
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);

        // Display the window
        frame.setSize(800, 600);
        frame.setVisible(true);
    }

    public static void main(String[] args) {

        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(RiverGUI::createAndShowGUI);
    }


    @Override
    public void mouseClicked(MouseEvent e) {
        if (gameOver()) {
            if (this.farmerGameButtonRect.contains(e.getPoint())) {
                engine = new FarmerGameEngine();
                repaint();
            }
            if (this.monsterGameButtonRect.contains(e.getPoint())) {
                engine = new MonsterGameEngine();
                repaint();
            }
            return;
        }
        if (item0Rectangle.contains(e.getPoint())) {
            if (engine.getItemLocation(Item.ITEM_0).isOnBoat()) {
                engine.unloadBoat(Item.ITEM_0);
            } else {
                engine.loadBoat(Item.ITEM_0);
            }
        } else if (item1Rectangle.contains(e.getPoint())) {
            if (engine.getItemLocation(Item.ITEM_1).isOnBoat()) {
                engine.unloadBoat(Item.ITEM_1);
            } else {
                engine.loadBoat(Item.ITEM_1);
            }
        } else if (item2Rectangle.contains(e.getPoint())) {
            if (engine.getItemLocation(Item.ITEM_2).isOnBoat()) {
                engine.unloadBoat(Item.ITEM_2);
            } else {
                engine.loadBoat(Item.ITEM_2);
            }
        } else if (item3Rectangle.contains(e.getPoint())) {
            if (engine.getItemLocation(Item.ITEM_3).isOnBoat()) {
                engine.unloadBoat(Item.ITEM_3);
            } else {
                engine.loadBoat(Item.ITEM_3);
            }
        } else if (item4Rectangle.contains(e.getPoint())) {
            if (engine.getItemLocation(Item.ITEM_4).isOnBoat()) {
                engine.unloadBoat(Item.ITEM_4);
            } else {
                engine.loadBoat(Item.ITEM_4);
            }
        } else if (item5Rectangle.contains(e.getPoint())) {
            if (engine.getItemLocation(Item.ITEM_5).isOnBoat()) {
                engine.unloadBoat(Item.ITEM_5);
            } else {
                engine.loadBoat(Item.ITEM_5);
            }
        } else if (boatRectangle.contains(e.getPoint())) {
            engine.rowBoat();
        }
        repaint();
    }

    // ----------------------------------------------------------
    // None of these methods will be used
    // ----------------------------------------------------------

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}