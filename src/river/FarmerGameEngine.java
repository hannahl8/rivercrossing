package river;

import java.awt.*;

public class FarmerGameEngine extends AbstractGameEngine {

    public static final Item BEANS = Item.ITEM_0;
    public static final Item GOOSE = Item.ITEM_1;
    public static final Item WOLF = Item.ITEM_2;
    public static final Item FARMER = Item.ITEM_3;

    public FarmerGameEngine() {
        super();
        gameObjects.put(BEANS, new GameObject("B", Location.START, Color.CYAN, false));
        gameObjects.put(GOOSE, new GameObject("G", Location.START, Color.CYAN, false));
        gameObjects.put(WOLF, new GameObject("W", Location.START, Color.CYAN, false));
        gameObjects.put(FARMER, new GameObject("", Location.START, Color.MAGENTA, true));
    }

    @Override
    public boolean gameIsLost() {
        if (gameObjects.get(GOOSE).getLocation() == Location.BOAT) {
            return false;
        }
        if (gameObjects.get(GOOSE).getLocation() == gameObjects.get(FARMER).getLocation()) {
            return false;
        }
        if (gameObjects.get(GOOSE).getLocation() == getBoatLocation()) {
            return false;
        }
        if (gameObjects.get(GOOSE).getLocation() == gameObjects.get(WOLF).getLocation()) {
            return true;
        }
        return gameObjects.get(GOOSE).getLocation() == gameObjects.get(BEANS).getLocation();
    }
}