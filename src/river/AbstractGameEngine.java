package river;

import java.awt.*;
import java.util.EnumMap;
import java.util.List;

public abstract class AbstractGameEngine implements GameEngine {

    protected EnumMap<Item, GameObject> gameObjects;
    private Location boatLocation;

    protected AbstractGameEngine() {
        gameObjects = new EnumMap<>(Item.class);
        this.boatLocation = Location.START;
    }

    @Override
    public String getItemLabel(Item item) {
        return gameObjects.get(item).getLabel();
    }

    @Override
    public Color getItemColor(Item item) {
        return gameObjects.get(item).getColor();
    }

    @Override
    public boolean getItemIsDriver(Item item) {
        return gameObjects.get(item).isDriver();
    }

    @Override
    public Location getItemLocation(Item item) {
        return gameObjects.get(item).getLocation();
    }

    @Override
    public void setItemLocation(Item item, Location location) {
        gameObjects.get(item).setLocation(location);
    }

    @Override
    public Location getBoatLocation() {
        return boatLocation;
    }

    @Override
    public void loadBoat(Item item) {
        int count = 0;
        for (Item i : getItems()) {
            if (getItemLocation(i).isOnBoat()) {
                count++;
            }
        }
        if (count == 2) {
            return;
        }
        if (gameObjects.get(item).getLocation() == getBoatLocation()) {
            gameObjects.get(item).setLocation(Location.BOAT);
        }
    }

    @Override
    public void unloadBoat(Item item) {
        if (gameObjects.get(item).getLocation().isOnBoat()) {
            gameObjects.get(item).setLocation(getBoatLocation());
        }
    }

    @Override
    public void rowBoat() {
        boolean isDriverOnBoat = false;
        for (Item item : getItems()) {
            if (getItemLocation(item).isOnBoat() && getItemIsDriver(item)) {
                isDriverOnBoat = true;
            }
        }
        if (!isDriverOnBoat) {
            return;
        }
        if (getBoatLocation() == Location.START) {
            boatLocation = Location.FINISH;
        } else {
            boatLocation = Location.START;
        }
    }

    @Override
    public boolean gameIsWon() {
        for (GameObject gameObject : gameObjects.values()) {
            if (gameObject.getLocation() != Location.FINISH) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean gameIsLost() {
        // Keep the gameIsLost method abstract and implement it in your concrete classes.
        return false;
    }

    @Override
    public void resetGame() {
        for (GameObject gameObject : gameObjects.values()) {
            gameObject.setLocation(Location.START);
        }
        boatLocation = Location.START;
    }

    @Override
    public List<Item> getItems() {
        return List.copyOf(gameObjects.keySet());
    }
}
